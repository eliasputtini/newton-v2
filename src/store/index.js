import {createStore, combineReducers} from 'redux';

import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {};

const questionArray = [];

const lawArray = [];

import database from '@react-native-firebase/database';

function counter(state = {total: 0}, action) {
  switch (action.type) {
    case 'INCREMENT':
      return {...state, total: action.payload + 1};
    case 'DECREMENT':
      return {...state, total: action.payload - 1};

    default:
      return state;
  }
}

function testSelect(state = {testNum: 0}, action) {
  switch (action.type) {
    case 'TEST_ZERO':
      return {...state, testNum: 0};
    case 'TEST_ONE':
      return {...state, testNum: 1};
    case 'TEST_TWO':
      return {...state, testNum: 2};
    default:
      return state;
  }
}

function questionReducer(state = questionArray, action) {
  switch (action.type) {
    case 'NEW_QUESTION':
      database()
        .ref('/questions')
        .set(action.payload)
        .then(() => console.log('Data set. qst'));

      AsyncStorage.setItem('NEWTON_APP', JSON.stringify(action.payload));
      console.log('async saved');
      return {
        ...state,
        questionArray: action.payload,
      };

    case 'LOAD_ASYNC':
      AsyncStorage.setItem('NEWTON_APP', JSON.stringify(action.payload));
      console.log('async saved');
      return {
        ...state,
        questionArray: action.payload,
      };

    case 'REMOVE_ALL':
      AsyncStorage.removeItem('NEWTON_APP');
      console.log('ALL questions deleted');
      return {
        ...state,
        questionArray: [],
      };

    default:
      return state;
  }
}

function lawReducer(state = lawArray, action) {
  switch (action.type) {
    case 'NEW_QUESTION_LAW':
      database()
        .ref('/legislacao')
        .set(action.payload)
        .then(() => console.log('Data set. law '));

      AsyncStorage.setItem('NEWTON_LAW', JSON.stringify(action.payload));
      console.log('async law saved');
      return {
        ...state,
        lawArray: action.payload,
      };

    case 'LOAD_ASYNC_LAW':
      AsyncStorage.setItem('NEWTON_LAW', JSON.stringify(action.payload));
      console.log('async law saved');
      return {
        ...state,
        lawArray: action.payload,
      };

    case 'REMOVE_ALL_LAW':
      AsyncStorage.removeItem('NEWTON_LAW');
      console.log('ALL questions deleted');
      return {
        ...state,
        lawArray: [],
      };

    default:
      return state;
  }
}

const rootReducer = combineReducers({
  counter,
  questionReducer,
  lawReducer,
  testSelect,
});

let store = createStore(rootReducer);

export default store;
