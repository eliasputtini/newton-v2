import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  bIcon: {
    width: '100%',
    flex: 1,
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    paddingHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  buttonText: {
    paddingHorizontal: 10,
    color: 'white',
    fontSize: 14,
  },
  disabledButton: {
    opacity: 0.6,
    width: '100%',
    flex: 1,
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    paddingHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
