import React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import styles from './styles';

export default function buttons({name, iconName, disabled, buttonPress}) {
  return (
    <>
      <TouchableOpacity
        disabled={disabled}
        style={disabled ? styles.disabledButton : styles.bIcon}
        onPress={buttonPress}>
        <View style={styles.viewRow}>
          <Icon name={iconName} color="white" size={50} />
          <Text style={styles.buttonText}>{name}</Text>
        </View>

        <Icon name="arrow-right" color="white" size={40} />
      </TouchableOpacity>
    </>
  );
}
