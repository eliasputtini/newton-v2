import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const AppStack = createStackNavigator();

import Home from './pages/Home';
import Detail from './pages/Detail';
import Test from './pages/Test';
import Editor from './pages/Editor';
import Deleta from './pages/Editor/Deleta';
import Adiciona from './pages/Editor/Adiciona';

export default function Routes() {
  return (
    <NavigationContainer>
      <AppStack.Navigator screenOptions={{headerShown: false}}>
        <AppStack.Screen name="Home" component={Home} />
        <AppStack.Screen name="Detail" component={Detail} />
        <AppStack.Screen name="Editor" component={Editor} />
        <AppStack.Screen name="Deleta" component={Deleta} />
        <AppStack.Screen name="Test" component={Test} />
        <AppStack.Screen name="NewQuest" component={Adiciona} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
}
