import React, {useEffect, useState, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  Image,
  Alert,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  StatusBar,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import logoImg from '../../assets/logo.png';
import messages from '../../assets/messages.json';

import styles from './styles';

import database from '@react-native-firebase/database';

import Button from '../../components/buttons';

export default function Home() {
  const {questionArray} = useSelector((state) => ({
    ...state.questionReducer,
  }));
  const {lawArray} = useSelector((state) => ({
    ...state.lawReducer,
  }));

  const {total} = useSelector((state) => ({...state.counter}));

  const [question, setQuest] = useState('');
  const [answer, setAnswer] = useState('');
  const [disableTest, setDisableTest] = useState(total >= 10 ? false : true);
  const [id, setId] = useState(
    questionArray !== undefined ? questionArray.length : 0,
  );

  const dispatch = useDispatch();
  const navigation = useNavigation();

  useEffect(() => {
    Notifications();
    LoadAsync();
  }, []);

  useEffect(() => {
    Counter();
    setDisableTest(total > 10 ? false : true);
  }, [questionArray, lawArray, total, Counter]);

  const Counter = useCallback(() => {
    let count = 0;

    if (questionArray !== undefined) {
      count += questionArray.length;
    }

    if (lawArray !== undefined) {
      count += lawArray.length;
    }

    dispatch({type: 'INCREMENT', payload: count - 1});
  }, [questionArray, lawArray, dispatch]);

  function LoadData() {
    database()
      .ref('/questions/')
      .once('value')
      .then((snapshot) => {
        if (snapshot.val() !== null) {
          console.log('User data: ', snapshot.val());

          dispatch({
            type: 'NEW_QUESTION',
            payload: snapshot.val(),
          });
        }
      });

    database()
      .ref('/legislacao/')
      .once('value')
      .then((snapshot) => {
        if (snapshot.val() !== null) {
          console.log('law qst: ', snapshot.val());

          dispatch({
            type: 'NEW_QUESTION_LAW',
            payload: snapshot.val(),
          });
        }
      });
  }

  async function LoadAsync() {
    try {
      let x = await AsyncStorage.getItem('NEWTON_APP');

      let y = await AsyncStorage.getItem('NEWTON_LAW');
      if (x != null) {
        console.log('quest async loaded ');
        dispatch({
          type: 'LOAD_ASYNC',
          payload: JSON.parse(x),
        });
      }
      if (y != null) {
        console.log('law async loaded ');
        dispatch({
          type: 'LOAD_ASYNC_LAW',
          payload: JSON.parse(y),
        });
      }
    } catch (e) {
      alert(e);
    }
    LoadData();
  }

  function Test() {
    if (questionArray !== undefined) {
      navigation.navigate('Test');
    }
  }

  var PushNotification = require('react-native-push-notification');

  function Notifications() {
    PushNotification.cancelAllLocalNotifications();

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: messages[Math.floor(Math.random() * 6)], // (required)
      icon: 'ic_notification',
      color: '#e85d04',
      date: new Date(Date.now()),
      repeatType: 'day',
    });
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#f0f0f5" />
      <View style={styles.viewLogo}>
        <Image source={logoImg} style={styles.img} />
      </View>

      <View style={styles.header}>
        <Text style={styles.title}>Bem-vindo!</Text>
        <Text style={styles.description}>
          Total de <Text style={styles.bold}>{total} perguntas!</Text>
        </Text>
      </View>

      <View style={styles.viewBotoes}>
        <View
          style={[
            disableTest ? {opacity: 0.6} : {opacity: 1},
            {height: '50%'},
          ]}>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            disableIntervalMomentum={true}
            snapToInterval={Dimensions.get('screen').width - 101}
            decelerationRate="fast">
            <TouchableOpacity
              activeOpacity={1}
              disabled={disableTest}
              style={styles.carouselItem}
              onPress={() => {
                Test();
                dispatch({
                  type: 'TEST_ZERO',
                });
              }}>
              <View style={styles.viewRow}>
                <Icon
                  name="file-document-edit-outline"
                  color="white"
                  size={50}
                />
                <Text style={styles.buttonText}>
                  Teste Rápido{'\n'}
                  <Text style={styles.buttonDescription}>
                    <Text style={styles.bold}>10</Text> perguntas de{'\n'}
                    categorias aleatórias
                  </Text>
                </Text>
              </View>

              <Icon name="arrow-right" color="white" size={40} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              disabled={disableTest}
              style={styles.carouselItem}
              onPress={() => {
                Test();
                dispatch({
                  type: 'TEST_ONE',
                });
              }}>
              <View style={styles.viewRow}>
                <Icon
                  name="file-document-edit-outline"
                  color="white"
                  size={50}
                />
                <Text style={styles.buttonText}>Teste Rápido</Text>
              </View>

              <Icon name="arrow-right" color="white" size={40} />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              disabled={disableTest}
              style={styles.carouselLastItem}
              onPress={() => {
                Test();
                dispatch({
                  type: 'TEST_TWO',
                });
              }}>
              <View style={styles.viewRow}>
                <Icon
                  name="file-document-edit-outline"
                  color="white"
                  size={50}
                />
                <Text style={styles.buttonText}>Teste Rápido</Text>
              </View>

              <Icon name="arrow-right" color="white" size={40} />
            </TouchableOpacity>
          </ScrollView>
        </View>

        <Button
          name="Estudar gabarito"
          iconName="book-open-page-variant"
          buttonPress={() => navigation.navigate('Detail')}
        />

        <Button
          name="Configurações"
          iconName="wrench"
          buttonPress={() => navigation.navigate('Editor')}
        />
      </View>
    </View>
  );
}
