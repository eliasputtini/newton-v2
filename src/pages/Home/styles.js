import {StyleSheet, Dimensions} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },

  bold: {
    fontWeight: 'bold',
  },

  title: {
    fontSize: 20,
    color: '#13131a',
    fontWeight: '500',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  header: {
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginBottom: 10,
  },

  description: {
    fontSize: 14,
    marginHorizontal: 5,
    color: '#737380',
  },
  img: {
    height: 200,
    width: '90%',
    resizeMode: 'contain',
  },
  viewLogo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  viewBotoes: {
    flex: 4,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 50,
  },

  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  carouselItem: {
    width: Dimensions.get('screen').width - 90,
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 5,
    marginRight: 10,
    paddingHorizontal: (Dimensions.get('screen').width - 90) / 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  carouselLastItem: {
    width: Dimensions.get('screen').width - 90,
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 5,
    paddingHorizontal: (Dimensions.get('screen').width - 90) / 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  buttonText: {
    paddingHorizontal: 10,
    color: 'white',
    fontSize: 15,
  },
  buttonDescription: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    color: 'white',
    fontSize: 14,
  },
});
