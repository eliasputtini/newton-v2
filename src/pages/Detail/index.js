import React, {useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {useSelector} from 'react-redux';
import logoImg from '../../assets/logo.png';
import styles from './styles';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

export default function Detail() {
  const {total} = useSelector((state) => ({...state.counter}));
  const {questionArray} = useSelector((state) => ({
    ...state.questionReducer,
  }));
  const {lawArray} = useSelector((state) => ({
    ...state.lawReducer,
  }));
  const [categoria, setCategoria] = useState('RACIOCÍNIO LÓGICO');
  const navigation = useNavigation();
  const route = useRoute();

  const pickerValues = [
    {
      title: 'RACIOCÍNIO LÓGICO',
    },
    {
      title: 'DIREITO ADMINISTRATIVO',
    },
  ];

  const [picker, setPicker] = useState('RACIOCÍNIO LÓGICO');

  const [pickerDisplayed, setPickerDisplayed] = useState(false);

  const togglePicker = () =>
    setPickerDisplayed((previousState) => !previousState);

  function setPickerValue(newValue) {
    setPicker(newValue);

    togglePicker();
  }

  function Select(picker) {
    if (picker === 'RACIOCÍNIO LÓGICO') {
      return questionArray;
    }
    if (picker === 'DIREITO ADMINISTRATIVO') {
      return lawArray;
    }
  }

  function printQuestion(array) {
    if (array !== undefined) {
      return array.map((data, i) => {
        return (
          <View key={i} style={styles.incident}>
            <Text style={[styles.incidentProperty, {marginTop: 0}]}>
              ID: {data.id}
              {'\n\n'}
              Questão: {data.question}
              {'\n\n'}
              Resposta: {data.answer ? 'Verdadeiro' : 'Falso'}
              {'\n\n'}
              {data.exp ? 'Explicação: ' + data.exp : ''}
            </Text>
          </View>
        );
      });
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="arrow-left" size={30} color="black" />
        </TouchableOpacity>
        <Image source={logoImg} />
      </View>
      <View style={styles.titleView}>
        <Text style={styles.title}>Respostas:</Text>
        <Text style={styles.description}>
          Gabarito de
          <Text style={styles.headerTextBold}>{' ' + categoria} </Text>
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => togglePicker()}
        style={styles.buttonModal}>
        <Text style={styles.buttonText}>{picker}</Text>
      </TouchableOpacity>
      <Modal
        visible={pickerDisplayed}
        animationType={'fade'}
        transparent={true}
        statusBarTranslucent={true}>
        <TouchableOpacity
          onPress={() => togglePicker()}
          style={{
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.5)',
            height: '100%',
            zIndex: -1,
          }}>
          <View style={styles.dropDown}>
            <Text style={styles.modalTitle}>Escolha uma matéria!</Text>
            {pickerValues.map((value, index) => {
              return (
                <TouchableHighlight
                  underlayColor="#DDDDDD"
                  key={index}
                  onPress={() => setPickerValue(value.title)}
                  style={{
                    paddingTop: 4,
                    paddingBottom: 4,
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <Text>{value.title}</Text>
                </TouchableHighlight>
              );
            })}

            <TouchableOpacity
              onPress={() => togglePicker()}
              style={{paddingTop: 4, paddingBottom: 4}}>
              <Text style={{color: '#999'}}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
      <ScrollView showsVerticalScrollIndicator={false}>
        {printQuestion(Select(picker))}
      </ScrollView>
    </View>
  );
}
