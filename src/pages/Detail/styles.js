import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  title: {
    fontSize: 20,
    marginBottom: 10,
    marginTop: 10,
    color: '#13131a',
    fontWeight: '500',
  },

  description: {
    fontSize: 14,
    color: '#737380',
  },

  incident: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#FFF',
    marginBottom: 16,
  },

  incidentProperty: {
    fontSize: 14,
    color: '#41414d',
    fontWeight: 'bold',
    marginTop: 24,
  },

  incidentValue: {
    marginTop: 8,
    fontSize: 15,
    color: '#737380',
  },

  titleText: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#13131a',
    lineHeight: 30,
  },

  titleDescription: {
    fontSize: 15,
    color: '#737380',
    marginTop: 5,
  },

  actions: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  action: {
    backgroundColor: '#e02041',
    borderRadius: 8,
    height: 50,
    width: '48%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bIcon: {
    width: '100%',
    height: '25%',
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 10,
    paddingHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  buttonText: {
    color: 'white',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '500',
  },
  buttonModal: {
    width: '100%',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 10,
    position: 'relative',
  },

  dropDown: {
    marginHorizontal: 23,
    padding: 20,
    backgroundColor: '#efefef',
    top: 66,
    alignItems: 'center',
    zIndex: 99,
  },

  modalTitle: {
    fontWeight: 'bold',
    marginBottom: 10,
  },

  viewBotoes: {
    paddingHorizontal: 24,
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  actionText: {
    color: '#FFF',
    fontSize: 15,
    fontWeight: 'bold',
  },
});
