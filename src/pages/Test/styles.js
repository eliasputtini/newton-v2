import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },

  plot: {
    width: '100%',
    alignItems: 'center',
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
  },

  titleView: {
    marginBottom: 10,
  },

  titleText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#13131a',
  },

  titleDescription: {
    fontSize: 14,
    color: '#737380',
  },

  question: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#FFF',
    marginBottom: 16,
  },

  questionTrue: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#2ECC71',
    marginBottom: 16,
  },

  questionFalse: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#e02041',
    marginBottom: 16,
  },

  questionText: {
    fontSize: 14,
    color: 'black',
    marginTop: 0,
  },
  switchText: {
    color: 'black',
    marginTop: 5,
    fontSize: 14,
  },

  switchView: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    //width: 300,
  },

  questionValue: {
    marginTop: 8,
    fontSize: 15,
    color: '#737380',
  },

  bIcon: {
    width: 170,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#e02041',
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  viewVoltar: {alignItems: 'flex-end'},

  buttonText: {
    color: 'white',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '500',
  },
});
