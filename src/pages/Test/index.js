import React, {useState, useEffect} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  Dimensions,
  View,
  Image,
  Text,
  TouchableOpacity,
  Switch,
  ScrollView,
} from 'react-native';

import {useSelector} from 'react-redux';

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {VictoryPie} from 'victory-native';

import logoImg from '../../assets/logo.png';

import styles from './styles';

const valorInicial = [{y: 0}, {y: 1}];
var wantedGraphicData = [];

Dimensions;

export default function Test() {
  const {testNum} = useSelector((state) => ({
    ...state.testSelect,
  }));
  const {questionArray} = useSelector((state) => ({
    ...state.questionReducer,
  }));
  const {lawArray} = useSelector((state) => ({
    ...state.lawReducer,
  }));

  const [answerBool, setAnswerBool] = useState([]);

  const [newArray, setNewArray] = useState([]);

  const [graphicData, setGraphicData] = useState(valorInicial);

  const [points, setPoints] = useState(0);

  const [testEnd, setTestEnd] = useState(false);

  const navigation = useNavigation();
  const route = useRoute();

  useEffect(() => {
    if (testNum === 0) {
      testZero();
    }

    if (testNum === 1) {
      setNewArray(JSON.parse(JSON.stringify(lawArray)));
    }
    switchArray();
  }, []);

  useEffect(() => {
    testEnd ? setGraphicData(wantedGraphicData) : '';
  }, [testEnd]);

  function setSwitch(i) {
    let result = answerBool.find((item, index) => index === i);
    answerBool.splice(i, 1);
    answerBool.splice(i, 0, !result);
    setAnswerBool([...answerBool]);
  }

  function Shuffle(unshuffled) {
    let shuffled = [];
    shuffled = unshuffled
      .map((a) => ({sort: Math.random(), value: a}))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value);

    return shuffled;
  }

  function testZero() {
    let shuffledArray = [];
    let randomArray = [];

    shuffledArray = Shuffle(questionArray);

    for (let i = 0; i < 5; i++) {
      randomArray.splice(i, 0, shuffledArray[i]);
    }

    shuffledArray = Shuffle(lawArray);

    for (let i = 0; i < 5; i++) {
      randomArray.splice(i, 0, shuffledArray[i]);
    }

    randomArray = Shuffle(randomArray);

    console.log(randomArray);

    setNewArray(JSON.parse(JSON.stringify(randomArray)));
  }

  function Plot() {
    let counter = 0;
    newArray.map((data, i) => {
      answerBool[i] === data.answer ? counter++ : '';
    });
    setPoints(counter);
    setTestEnd(true);
    wantedGraphicData = [
      {
        x: `Acertos:\n ${(counter / newArray.length).toFixed(2) * 100}%`,
        y: counter,
      },
      {
        x: `Erros:\n ${
          ((newArray.length - counter) / newArray.length).toFixed(2) * 100
        }%`,
        y: newArray.length - counter,
      },
    ];
  }

  function switchArray() {
    if (testNum === 0) {
      for (let i = 0; i < 10; i++) {
        setAnswerBool((answerBool) => [...answerBool, false]);
      }
      console.log('useEffect:answerBool[] atualizado');
    }
    if (testNum === 1) {
      lawArray.map((data, id) => {
        setAnswerBool((answerBool) => [...answerBool, false]);
      });
      console.log('useEffect:answerBool[] atualizado');
    }
  }

  function printResults() {
    return newArray.map((data, i) => {
      return (
        <View
          key={i}
          style={
            answerBool[i] === newArray[i].answer
              ? styles.questionTrue
              : styles.questionFalse
          }>
          <Text style={styles.questionText}>
            Questão: {data.question}
            {'\n\n'}
            Gabarito: {data.answer ? 'Certo' : 'Errado'}
          </Text>

          <View style={styles.switchView}>
            <Text style={styles.switchText}>Errado</Text>
            <Switch
              trackColor={{false: '#463f3a', true: '#463f3a'}}
              thumbColor={answerBool[i] ? '#f2efe6' : '#f2efe6'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={() => {}}
              value={answerBool[i]}
            />
            <Text style={styles.switchText}>Certo</Text>
          </View>
          <View style={styles.switchView}>
            <Icon
              name={answerBool[i] === newArray[i].answer ? 'check' : 'close'}
              color={answerBool[i] === newArray[i].answer ? 'green' : '#641220'}
              size={40}
            />

            <Text style={styles.switchText}>
              {answerBool[i] === newArray[i].answer ? 'Acertou' : 'Errou'}
            </Text>
          </View>
          <Text style={styles.switchText}>
            {data.exp ? 'Explicação: ' + data.exp : ''}
          </Text>
        </View>
      );
    });
  }

  function testMap() {
    return newArray.map((data, i) => {
      return (
        <View key={i} style={styles.question}>
          <Text style={styles.questionText}>
            Questão: {data.question}
            {'\n\n'}
          </Text>
          <View style={styles.switchView}>
            <Text style={styles.switchText}>Errada</Text>
            <Switch
              trackColor={{false: '#463f3a', true: '#463f3a'}}
              thumbColor={answerBool[i] ? '#f2efe6' : '#f2efe6'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={() => setSwitch(i)}
              value={answerBool[i]}
            />
            <Text style={styles.switchText}>Certa</Text>
          </View>
        </View>
      );
    });
  }

  return (
    <>
      {!testEnd && (
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.pop()}>
              <Icon name="arrow-left" size={30} color="black" />
            </TouchableOpacity>
            <Image source={logoImg} />
          </View>

          <View style={styles.titleView}>
            <Text style={styles.titleText}>Teste Rápido:</Text>
            <Text style={styles.titleDescription}>
              Marque Verdadeiro ou Falso
            </Text>
          </View>

          <View>{testMap()}</View>

          <TouchableOpacity
            style={styles.bIcon}
            onPress={() => {
              Plot();
            }}>
            <Icon name="check" color="white" size={30} />
            <Text style={styles.buttonText}>Confirmar</Text>
          </TouchableOpacity>
        </ScrollView>
      )}
      {testEnd && (
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.container}>
          <View style={styles.header}>
            <Image source={logoImg} />
          </View>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>Resultados:</Text>
            <Text style={styles.titleDescription}>
              Acertos: {points}/{questionArray.length}
            </Text>
          </View>

          <View style={styles.plot}>
            <VictoryPie
              animate={{duration: 1000, easing: 'bounce'}}
              data={graphicData}
              width={Dimensions.get('screen').width}
              height={278}
              colorScale={['#2ECC71', '#e02041']}
              innerRadius={0}
            />
          </View>

          {printResults()}
          <View style={styles.viewVoltar}>
            <TouchableOpacity
              style={styles.bIcon}
              onPress={() => navigation.pop()}>
              <Icon name="arrow-left" color="white" size={40} />
              <Text style={styles.buttonText}>Voltar</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      )}
    </>
  );
}
