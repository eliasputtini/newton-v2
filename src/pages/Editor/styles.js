import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },

  headerTextBold: {
    fontWeight: 'bold',
  },

  title: {
    fontSize: 20,
    color: '#13131a',
    fontWeight: '500',
    marginVertical: 10,
  },
  ViewTitle: {
    flex: 1,
    justifyContent: 'center',
  },

  description: {
    fontSize: 14,
    lineHeight: 24,
    color: '#737380',
  },

  incident: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#FFF',
    marginBottom: 16,
  },

  incidentProperty: {
    fontSize: 14,
    color: '#41414d',
    fontWeight: 'bold',
  },

  incidentValue: {
    marginTop: 8,
    fontSize: 15,
    marginBottom: 24,
    color: '#737380',
  },

  logoText: {
    fontSize: 18,
    color: 'white',
  },
  inputBox: {
    fontFamily: 'Raleway-Medium',
    width: 300,
    backgroundColor: 'white',
    borderRadius: 25,
    paddingHorizontal: 16,
    marginVertical: 10,
    alignSelf: 'center',
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },

  viewBotoes: {
    flex: 3,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  bIcon: {
    width: '100%',
    height: '25%',
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 10,
    paddingHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  buttonText: {
    paddingHorizontal: 10,
    color: 'white',
    fontSize: 14,
  },

  textStyle: {
    fontSize: 16,
    fontWeight: '500',
    flexWrap: 'wrap',
  },
});
