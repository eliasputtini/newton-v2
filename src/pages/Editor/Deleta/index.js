import React, {useState} from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  View,
  Image,
  Alert,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  ScrollView,
  Button,
  Modal,
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';

import logoImg from '../../../assets/logo.png';

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import styles from './styles';

export default function Deleta() {
  const {questionArray} = useSelector((state) => ({
    ...state.questionReducer,
  }));
  const {lawArray} = useSelector((state) => ({
    ...state.lawReducer,
  }));

  const pickerValues = [
    {
      title: 'RACIOCÍNIO LÓGICO',
    },
    {
      title: 'DIREITO ADMINISTRATIVO',
    },
  ];

  const [picker, setPicker] = useState('RACIOCÍNIO LÓGICO');

  const [pickerDisplayed, setPickerDisplayed] = useState(false);

  const togglePicker = () =>
    setPickerDisplayed((previousState) => !previousState);

  function setPickerValue(newValue) {
    setPicker(newValue);

    togglePicker();
  }

  const {total} = useSelector((state) => ({...state.counter}));

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();

  function navigateBack() {
    navigation.goBack();
  }

  const [removeID, setRemoveID] = useState();

  function removeAll() {
    try {
      dispatch({type: 'REMOVE_ALL'});
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  function Select(picker) {
    if (picker === 'RACIOCÍNIO LÓGICO') {
      return questionArray;
    }
    if (picker === 'DIREITO ADMINISTRATIVO') {
      return lawArray;
    }
  }

  function removeQuestion(idx, array) {
    idx = parseInt(idx, 10);
    const result = array.find((obj) => obj.id === idx);

    console.log(array);
    if (result) {
      //se encontrou id

      //questionArray.splice(result.id, 1); //remove

      let newArray = JSON.parse(JSON.stringify(array));

      newArray.splice(result.id, 1);

      console.log(result, ' removido ');

      for (let i = 0; i < newArray.length; i++) {
        newArray[i].id = i;
      }

      if (picker === 'RACIOCÍNIO LÓGICO') {
        dispatch({
          type: 'NEW_QUESTION',
          payload: newArray,
        });
      }
      if (picker === 'DIREITO ADMINISTRATIVO') {
        dispatch({
          type: 'NEW_QUESTION_LAW',
          payload: newArray,
        });
      }
    } else {
      Alert.alert('Questão não encontrada!');
    }
  }

  function printQuestion(array) {
    console.log(lawArray);
    if (array !== undefined) {
      return array.map((data, i) => {
        return (
          <View key={i} style={styles.incident}>
            <Text style={[styles.incidentProperty, {marginTop: 0}]}>
              ID: {data.id}
              {'\n\n'}
              Questão: {data.question}
              {'\n\n'}
              Resposta: {data.answer ? 'Verdadeiro' : 'Falso'}
              {'\n\n'}
            </Text>
          </View>
        );
      });
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="arrow-left" size={30} color="black" />
        </TouchableOpacity>
        <Image source={logoImg} />
      </View>

      <View style={styles.titleView}>
        <Text style={styles.titleText}>Delete:</Text>
        <Text style={styles.titleDescription}>
          Digite o id e clique no botão para deletar
        </Text>
        <TouchableOpacity
          onPress={() => togglePicker()}
          style={styles.buttonModal}>
          <Text style={styles.buttonText}>{picker}</Text>
        </TouchableOpacity>
        <Modal
          visible={pickerDisplayed}
          animationType={'fade'}
          transparent={true}
          statusBarTranslucent={true}>
          <TouchableOpacity
            onPress={() => togglePicker()}
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: '100%',
              zIndex: -1,
            }}>
            <View style={styles.dropDown}>
              <Text style={styles.modalTitle}>Escolha uma matéria!</Text>
              {pickerValues.map((value, index) => {
                return (
                  <TouchableHighlight
                    underlayColor="#DDDDDD"
                    key={index}
                    onPress={() => setPickerValue(value.title)}
                    style={{
                      paddingTop: 4,
                      paddingBottom: 4,
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <Text>{value.title}</Text>
                  </TouchableHighlight>
                );
              })}

              <TouchableOpacity
                onPress={() => togglePicker()}
                style={{paddingTop: 4, paddingBottom: 4}}>
                <Text style={{color: '#999'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        {printQuestion(Select(picker))}
      </ScrollView>

      <TextInput
        style={styles.inputBox}
        placeholder={'Digite um ID'}
        Type={'String'}
        onChangeText={setRemoveID}
      />
      <View style={styles.viewBotoes}>
        <TouchableOpacity
          style={styles.bIcon}
          onPress={() => removeQuestion(removeID, Select(picker))}>
          <Icon name="pencil-remove-outline" size={30} color="white" />
          <Text style={styles.buttonText}>DELETA</Text>
        </TouchableOpacity>

        {/* <TouchableOpacity
          style={styles.bIcon}
          onPress={() => {
            console.log(questionArray);
            removeAll();
          }}>
          <Icon name="trash-can-outline" size={30} color="white" />
          <Text style={styles.buttonText}>DELETAR TUDO</Text>
        </TouchableOpacity> */}
      </View>
    </View>
  );
}
