import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },
  titleView: {
    marginBottom: 16,
  },

  titleText: {
    fontSize: 20,
    color: '#13131a',
  },

  titleDescription: {
    fontSize: 14,
    color: '#737380',
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },

  incident: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#FFF',
    marginBottom: 16,
  },

  incidentProperty: {
    fontSize: 14,
    color: '#41414d',
    marginTop: 24,
  },

  incidentValue: {
    marginTop: 8,
    fontSize: 15,
    color: '#737380',
  },

  viewBotoes: {
    flexDirection: 'row',

    justifyContent: 'center',
  },

  bIcon: {
    width: '100%',
    height: 50,
    borderRadius: 10,
    backgroundColor: '#e02041',
    marginBottom: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '500',
  },
  buttonModal: {
    width: '100%',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 10,
    position: 'relative',
  },

  dropDown: {
    marginHorizontal: 23,
    padding: 20,
    backgroundColor: '#efefef',
    top: 66,
    alignItems: 'center',
    zIndex: 99,
  },

  modalTitle: {
    fontWeight: 'bold',
    marginBottom: 10,
  },

  textStyle: {fontSize: 14},
});
