import React, {useState, useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  Switch,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  Button,
  Modal,
} from 'react-native';

import {useNavigation} from '@react-navigation/native';

import styles from './styles';
import logoImg from '../../../assets/logo.png';

const pickerValues = [
  {
    title: 'RACIOCÍNIO LÓGICO',
  },
  {
    title: 'DIREITO ADMINISTRATIVO',
  },
];

export default function NewQuest() {
  const {total} = useSelector((state) => ({...state.counter}));
  const {questionArray} = useSelector((state) => ({
    ...state.questionReducer,
  }));

  const {lawArray} = useSelector((state) => ({
    ...state.lawReducer,
  }));

  const [picker, setPicker] = useState('RACIOCÍNIO LÓGICO');

  const [pickerDisplayed, setPickerDisplayed] = useState(false);

  const [id, setId] = useState(
    questionArray !== undefined ? questionArray.length : 0,
  );

  const [question, setQuest] = useState('');
  const [exp, setExp] = useState('');
  const [answer, setAnswer] = useState(false);
  const toggleSwitch = () => setAnswer((previousState) => !previousState);

  const togglePicker = () =>
    setPickerDisplayed((previousState) => !previousState);

  function setPickerValue(newValue) {
    setPicker(newValue);

    togglePicker();
  }

  const dispatch = useDispatch();
  const navigation = useNavigation();

  function checaVazio() {
    if (question === '') {
      alert('Vazio!');
    } else {
      cadastra();
    }
  }

  function cadastra() {
    picker === 'RACIOCÍNIO LÓGICO'
      ? dispatch({
          type: 'NEW_QUESTION',
          payload:
            questionArray !== undefined
              ? [...questionArray, {id, answer, question, exp}]
              : [{id, answer, question, exp}],
        })
      : 0;

    picker === 'DIREITO ADMINISTRATIVO'
      ? dispatch({
          type: 'NEW_QUESTION_LAW',
          payload:
            lawArray !== undefined
              ? [...lawArray, {id, answer, question, exp}]
              : [{id, answer, question, exp}],
        })
      : 0;

    navigation.navigate('Editor');
  }

  useEffect(() => {
    if (picker === 'RACIOCÍNIO LÓGICO') {
      setId(questionArray.length);
    }
    if (picker === 'DIREITO ADMINISTRATIVO') {
      setId(lawArray.length);
    }
  }, [questionArray, lawArray, picker]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="arrow-left" size={30} color="black" />
        </TouchableOpacity>
        <Image source={logoImg} />
      </View>

      <View style={styles.body}>
        <Text>Escolha a matéria!</Text>
        <TouchableOpacity
          onPress={() => togglePicker()}
          style={styles.buttonModal}>
          <Text style={styles.buttonText}>{picker}</Text>
        </TouchableOpacity>

        <Modal
          visible={pickerDisplayed}
          animationType={'fade'}
          transparent={true}
          statusBarTranslucent={true}>
          <TouchableOpacity
            onPress={() => togglePicker()}
            style={{
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.5)',
              height: '100%',
              zIndex: -1,
            }}>
            <View style={styles.dropDown}>
              <Text style={styles.modalTitle}>Escolha uma matéria!</Text>
              {pickerValues.map((value, index) => {
                return (
                  <TouchableHighlight
                    underlayColor="#DDDDDD"
                    key={index}
                    onPress={() => setPickerValue(value.title)}
                    style={{
                      paddingTop: 4,
                      paddingBottom: 4,
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <Text>{value.title}</Text>
                  </TouchableHighlight>
                );
              })}

              <TouchableOpacity
                onPress={() => togglePicker()}
                style={{paddingTop: 4, paddingBottom: 4}}>
                <Text style={{color: '#999'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </Modal>

        <Text style={styles.textStyle}>Digite a Questão:{id}</Text>
        <SafeAreaView style={styles.fundo}>
          <TextInput
            style={styles.inputBox}
            placeholder={'Digite a questão aqui...'}
            multiline={true}
            Type={'String'}
            onChangeText={setQuest}
          />
          <TextInput
            style={styles.inputBox}
            placeholder={'Se desejar, digite a explicação aqui...'}
            multiline={true}
            Type={'String'}
            onChangeText={setExp}
          />
        </SafeAreaView>
        <View style={styles.switchView}>
          <Switch
            trackColor={{false: '#463f3a', true: '#463f3a'}}
            thumbColor={answer ? '#f2efe6' : '#f2efe6'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={answer}
          />
          <Text style={styles.switchText}>
            {answer ? 'Verdadeiro' : 'Falso'}
          </Text>
        </View>
        <TouchableOpacity style={styles.bIcon} onPress={() => checaVazio()}>
          <Icon name="check" color="white" size={30} />
          <Text style={styles.buttonText}>Confirmar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
