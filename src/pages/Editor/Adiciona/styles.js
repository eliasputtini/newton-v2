import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: 24,
  },
  body: {
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonModal: {
    width: '100%',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#e02041',
    paddingVertical: 10,
    marginVertical: 10,
    position: 'relative',
  },

  dropDown: {
    marginHorizontal: 23,
    padding: 20,
    backgroundColor: '#efefef',
    top: 66,
    alignItems: 'center',
    zIndex: 99,
  },

  modalTitle: {
    fontWeight: 'bold',
    marginBottom: 10,
  },

  textStyle: {fontSize: 14},

  fundo: {
    height: 300,
  },
  inputBox: {
    flex: 1,
    width: 300,
    backgroundColor: 'white',
    borderRadius: 25,
    paddingHorizontal: 16,
    marginVertical: 10,
  },
  switchView: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: 300,
  },
  switchText: {
    color: 'black',
    paddingHorizontal: 10,
    fontSize: 14,
    fontWeight: '500',
  },

  bIcon: {
    width: 170,
    height: 50,
    borderRadius: 10,
    backgroundColor: '#e02041',
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  buttonText: {
    color: 'white',
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '500',
  },
});
