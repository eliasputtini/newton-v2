import React, {useEffect, useState} from 'react';

import {useSelector, useDispatch} from 'react-redux';

import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import logoImg from '../../assets/logo.png';

import {useNavigation} from '@react-navigation/native';
import styles from './styles';

export default function Editor() {
  const {total} = useSelector((state) => ({...state.counter}));
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="arrow-left" size={30} color="black" />
        </TouchableOpacity>
        <Image source={logoImg} />
      </View>

      <View style={styles.ViewTitle}>
        <Text style={styles.title}>
          Total de <Text style={styles.headerTextBold}>{total} perguntas!</Text>
        </Text>
        <Text style={styles.description}>
          Clique nos botões para editar as questões.
        </Text>
      </View>

      <View style={styles.viewBotoes}>
        <TouchableOpacity
          style={styles.bIcon}
          onPress={() => navigation.navigate('NewQuest')}>
          <View style={styles.viewRow}>
            <Icon name="pencil-plus-outline" size={50} color="white" />
            <Text style={styles.buttonText}>Adicionar questão </Text>
          </View>
          <Icon name="arrow-right" color="white" size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.bIcon}
          onPress={() => navigation.navigate('Deleta')}>
          <View style={styles.viewRow}>
            <Icon name="pencil-remove-outline" size={50} color="white" />
            <Text style={styles.buttonText}>Remover questão</Text>
          </View>
          <Icon name="arrow-right" color="white" size={40} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.bIcon}
          onPress={() => navigation.navigate('Detail')}>
          <View style={styles.viewRow}>
            <Icon name="file-document-outline" size={50} color="white" />
            <Text style={styles.buttonText}>Gabarito de questões</Text>
          </View>
          <Icon name="arrow-right" color="white" size={40} />
        </TouchableOpacity>
      </View>
    </View>
  );
}
